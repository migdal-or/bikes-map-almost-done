//
//  BMPLoadStations.m
//  Bikes map try4
//
//  Created by Dmitry A. Zvorykin on 13/05/2017.
//  Copyright © 2017 Dmitry A. Zvorykin. All rights reserved.
//

#import "BMPLoadStations.h"

@implementation BMPLoadStations

static NSDictionary *parkings;

-(void)loadStations {
    // DONE TODO 1. Save stations object and return cached copy between calls if the object has already been retrieved
    if (nil == parkings) {
        // start getting stations from api or local file
        __block NSDictionary *local_parkings;
        NSMutableURLRequest *request;
        request = [NSMutableURLRequest
                   requestWithURL:[NSURL URLWithString:@"http://apivelobike.velobike.ru/ride/parkings"]
                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                   timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask;
        dataTask = [session dataTaskWithRequest:request
                              completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                  if (error) {
                                      NSLog(@"%@", error);
                                  } else {
                                      local_parkings = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                  }
                                  parkings = local_parkings;
                                  [_delegate stationsGotLoaded: parkings];
                              }];
        [dataTask resume];
    } else {    // относится к if (nil == parkings)
        // в словаре parkings уже были данные, значит их уже загружали, значит можно вернуть сохранённую копию.
        [_delegate stationsGotLoaded: parkings]; // надо обязательно возвращать результаты вот так или можно проще?
    }
}
@end
